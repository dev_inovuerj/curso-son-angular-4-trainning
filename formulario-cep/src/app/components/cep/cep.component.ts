import { Component, OnInit } from '@angular/core';
import { CepService } from '../../services/cep.service';
import { Cep } from './cep';

@Component({
  selector: 'app-cep',
  templateUrl: './cep.component.html',
  styleUrls: ['./cep.component.css']
})
export class CepComponent implements OnInit {


  cep = new Cep();

  isLoading = true; // be loading

  constructor(private cepService: CepService) { }

  ngOnInit() {
  
  	// Enable inputs after 2secs
  	setTimeout( ()=>{
  		this.isLoading = false; // not be loading
  	},2000);
  	
  }

  findCep(){

  	this.isLoading = true;

  	this.cepService.searchPostalCode(this.cep.cep)
  		// In arrow functions with typing, surround the parameter with paranteses
  		.then( (cepObject:Cep) => {
  			this.isLoading = false;
  			this.cep = cepObject;
  		})
  		.catch( ()=> {
  			let cep = this.cep.cep;
  			
  			this.cep = new Cep();

  			this.cep.cep = cep;
  			
  			alert('Não foi possível continuar a busca');

  			this.isLoading = false;
  		});
  }

}
