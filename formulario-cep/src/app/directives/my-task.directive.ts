/* Hostlistener - bind Events between element DOM and Directives */
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

export class Task{
  name: string;
  value: number;
  date_launch:string; // Date Inputing Stock
}

@Directive({
  selector: '[myTask]'
})
export class MyTaskDirective {

  private _myTask: Task;

  constructor(private el: ElementRef) { 
  	this.el.nativeElement.innerHTML = 'Conteudo inserido'
  }


  /****************************************************************************/
  /*Watching values with Encapsules of Methods Magicals (getters And Setters) */
  /****************************************************************************/
  get myTask(){
  	return this._myTask;
  }

  @Input() // pass value from template to directive
  set myTask(value:Task){
  	this._myTask = value;
  	this.changeColorTask(); // value initialize in red color
  }

  
  /* Bind Events*/
  @HostListener('click')
  onClick(){ alert(this.myTask); }


  /* Methods*/
  changeColorTask(){
  	this.el.nativeElement.style.color = this._myTask.value > 5 ? 'green' : 'red';
  }


  // @Input()
  // myTask:Task;


}
