/* 
Symbols from Angular Core 

Directive: provides the functionality of the @Directive decorator.
ElementRef: injects into the directive's constructor so the code can access the DOM element.

HostListener: Respond to user-initiated events. Allows you to listen for events in an element. 
Input: allows data to flow from the binding expression into the directive. 
*/
import { Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive(	
	{
		selector: '[myHighlight]' // Metadata 'Selector'. Customing a CSS selector for an attribute
	}
)
export class HighlightDirective implements OnInit {

	/*Constructor*/
	constructor(private el: ElementRef) {}

	/*Properties Inputs*/
	/*highlightColor as alias*/
	@Input('myHighlight') highlightColor: string; 

	/*Bind to a second property*/
	@Input() defaultColor: string;
	
	/*Events*/
	@HostListener('mouseenter') onMouseEnter() {
		// this.highlight(this.highlightColor);
		this.highlight(this.highlightColor || this.defaultColor || 'red');
	}

	@HostListener('mouseleave') onMouseLeave() {
		this.highlight(null);
	}

	/*Method Highlight*/
	private highlight(color: string){
		this.el.nativeElement.style.backgroundColor = color;
		this.el.nativeElement.className = 'break-line';
	}

	/*Hooks*/
	ngOnInit(){
		// from value of property color
		// this.highlight(this.highlightColor || 'red');
	}

}