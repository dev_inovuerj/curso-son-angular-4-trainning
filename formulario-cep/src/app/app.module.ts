import { BrowserModule } from '@angular/platform-browser';

// Package nG Core - @NgModule Decorator
import { NgModule } from '@angular/core';

// Package Forms - Added to apply binding, between property of forms and Components
import { FormsModule } from '@angular/forms';

// Package HTTP
import { HttpModule } from '@angular/http';

// Components Imported
import { AppComponent } from './app.component';
import { CepComponent } from './components/cep/cep.component';

// Filters Imported

// Directives Imported

// Services Imported
import { CepService } from './services/cep.service';


@NgModule({
  declarations: [
    AppComponent,
    CepComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  // services container
  providers: [CepService], 
  bootstrap: [AppComponent]
})
export class AppModule { }