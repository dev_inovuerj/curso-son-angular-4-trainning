import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Cep } from '../components/cep/cep';

@Injectable()
export class CepService {

  constructor(private httpService: Http) { }

  searchPostalCode(cep: string){
  	
  	return this.httpService.get(`https://viacep.com.br/ws/${cep}/json/`)
	  		.toPromise()
	  		.then( response => {
				
				console.log(response);

				return this.convertResponseToCep(response.json());

	  		})
	  		;
  }

  private convertResponseToCep(cepResponse):Cep{

  	let cep = new Cep();

  	cep.cep = cepResponse.cep;
  	cep.logradouro = cepResponse.logradouro;
  	cep.complemento = cepResponse.complemento;
  	cep.bairro = cepResponse.bairro;
  	cep.cidade = cepResponse.localidade;
  	cep.estado = cepResponse.uf;

  	return cep;

  }

}
