import { BrowserModule } from '@angular/platform-browser';

// nG Core - @NgModule Decorator
import { NgModule } from '@angular/core';

// Forms - Added to apply binding, between property of forms and Components
import { FormsModule } from '@angular/forms';

// Components Imported
import { AppComponent } from './app.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskNewComponent } from './components/task-new/task-new.component';

// Filters Imported
import { FormatCurrencyPipe } from './filters/format-currency.pipe';
import { FormatDatePipe } from './filters/format-date.pipe';

// Directives Imported
import { MyTaskDirective } from './directives/my-task.directive';
import { HighlightDirective } from './directives/highlight.directive';

// Routes
import { RouterModule, Routes } from '@angular/router';

// Services
import { TaskService } from './task.service';

const appRoutes:Routes = [
  {
    path: 'tasks', component: TaskListComponent    
  },
  {
    path: 'task/new', component: TaskNewComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    TaskNewComponent,
    FormatCurrencyPipe,
    FormatDatePipe,
    MyTaskDirective,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
