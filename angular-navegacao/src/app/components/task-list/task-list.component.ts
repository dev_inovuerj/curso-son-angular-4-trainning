import { Component, OnInit, Input } from '@angular/core';

// Or Using Common Pipe Date in : https://angular.io/api/common/DatePipe
import { DatePipe } from '@angular/common';

import { Task } from '../../task';

import { TaskService } from '../../task.service';

// Decorator Turn on Component
@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

	@Input() tasks;

  	task:Task = {name: '',value:0, date_launch:"2017-07-20"};

  	today:number = Date.now();


	// add($event){

	// 	$event.preventDefault();

	// 	// Creating object copy(new Reference). 
	// 	// New Object, same values
	// 	let taskCopy = Object.assign({},this.task);

	// 	this.tasks.push(taskCopy);

	// }

	constructor(private taskService: TaskService) { }

	ngOnInit() {
		
		console.log(this.tasks);

		/* Factoring Objects TaskService Manually without D.I (Bad Pattern) */
	  	this.tasks = this.taskService.tasks;

	  	// this.tasks.push({name:'Cozinhar', value:50, date_launch:'2017-07-07'})
	}

}
