// Decorations and Transform Class
import { Pipe, PipeTransform } from '@angular/core';

// Turn on a pipe |
@Pipe({
  name: 'formatCurrency' // name pipe. ex: <li {{item | formatCurrency}}>
})
// Implements Interface PipeTransform to override 'transform' method
export class FormatCurrencyPipe implements PipeTransform {

	/*
		returns pattern currency BRL - Brazilian Real
	 */
  transform(value: any, locale = 'pt-BR'): any {

  	let styleCurrency:Object = {};

  	styleCurrency['pt-BR'] = {style: 'currency', currency: 'BRL'};
  	styleCurrency['en-US'] = {style: 'currency', currency: 'USD'};

  	// ES 6 - Currency Formats (https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat)
  	return new Intl.NumberFormat(locale,styleCurrency[locale]).format(value);

    // return null;
  }

}
