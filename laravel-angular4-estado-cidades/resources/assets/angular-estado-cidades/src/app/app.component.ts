import { Component, OnInit } from '@angular/core';
import { CidadeService } from './cidade.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app Estados/Cidades';

  estados:Array<any>;
  cidades:Array<any>;
  cidadeSelected:number;

  constructor(private cidadeService:CidadeService){

  	// RJ
  	this.cidadeSelected = 19

  	this.cidadeService.getEstados()
  		.then( (estados:Array<any>)=>{
  			this.estados = estados;
  		});


	this.cidadeService.getCidades(this.cidadeSelected)
  		.then( (cidades:Array<any>)=>{
  			this.cidades = cidades;
  		});



  }

  ngOnInit(){
  	console.log(this.cidadeSelected);	
  }

  

  getCidades(){

  	this.cidadeService.getCidades(this.cidadeSelected)
  		.then( (cidades:Array<any>)=>{
  			this.cidades = cidades;
  		});

  }

}