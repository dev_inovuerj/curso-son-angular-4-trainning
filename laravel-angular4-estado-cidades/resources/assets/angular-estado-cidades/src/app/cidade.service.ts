import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CidadeService {

  	constructor(private http: Http) {}

  	getEstados():Promise<Array<any>>{
		return this.http.get('http://127.0.0.1:8000/estados')
			.toPromise()
			.then( response=>response.json() );
	}

  	getCidades(estadoId:number){
  		return this.http.get(`http://127.0.0.1:8000/estados/${estadoId}/cidades`)
			.toPromise()
			.then( response=>response.json() );
  	}

}
