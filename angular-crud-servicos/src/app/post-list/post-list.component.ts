import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PostService} from '../services/post.service';
import {ModalComponent} from "../bootstrap/modal/modal.component";
import {MessageService} from "../services/message.service";

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

    posts: any = [];
    postToDelete = null; // store post clicked in the *ngFor posts

    // ViewChild receive a Component
    @ViewChild(ModalComponent)
    modal: ModalComponent;

    @ViewChild('btnDelete')
    btnDelete: ElementRef;

    loading = 'Carregando';
    isLoading = true;

    message: string;

    constructor(private postService: PostService,
                private messageService: MessageService) {
    }

    ngOnInit() {

        this.message = this.messageService.body;

        this.postService.query()
        // inscrever-se
            .subscribe(data => {
                // loading effect
                window.setTimeout(() => {
                    this.loading = '';
                    this.isLoading = false;
                    this.posts = data;
                }, 500);

            });
    }


    /* // Destroy by window.confirm
    destroy(id: number, index: number) {

        if (confirm('Do you really want to delete?')) {

            this.postService.destroy(+id)
                .subscribe(() => {
                    this.posts.splice(index, 1);
                    alert('Post Deleted!');
                });
        }

    }
    */


    /*Modal Methods*/

    openModal(post: any) {

        this.btnDelete.nativeElement.innerHTML = 'Delete';

        // post to Delete
        this.postToDelete = post; // This will run last. Digestion cycle of the ANGULAR
        // console.log(this.postToDelete);

        // Call modal bootstrap
        this.modal.open();

    }

    destroyByModal(post) {

        this.btnDelete.nativeElement.innerHTML = 'Carregando';

        this.postService.destroy(+post.id)
            .subscribe(() => {

                const indexToRemove = this.posts.indexOf(post);
                this.posts.splice(indexToRemove, 1);

                this.modal.close();
                this.message = "Post Deleted successful";
            });
    }

}
