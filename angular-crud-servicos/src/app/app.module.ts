// Modules Imports
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core'; // @NgModule from Package Core
import {RouterModule, Routes} from '@angular/router'; // Routes Module from package Router
import {HttpClientModule} from '@angular/common/http'; // HttpClientModule from @angular/common/http
import {FormsModule} from '@angular/forms';
// Services Imports
import {PostService} from './services/post.service';
// Components Imports
import {AppComponent} from './app.component';
import {PostListComponent} from './post-list/post-list.component';
import {PostSaveComponent} from './post-save/post-save.component';
import {ButtonComponent} from './bootstrap/button/button.component';
import {GlyphComponent} from './bootstrap/glyph/glyph.component';
import {ModalComponent} from './bootstrap/modal/modal.component';
import { AlertComponent } from './bootstrap/alert/alert.component';
import {MessageService} from "./services/message.service";

// Routes > Configs
const appRoutes: Routes = [
    {path: '', pathMatch: 'full', component: PostListComponent}, // with pathMatch full
    {path: 'posts', component: PostListComponent},
    {path: 'posts/create', component: PostSaveComponent},
    {path: 'posts/:id/edit', component: PostSaveComponent},
];

@NgModule({
    declarations: [
        AppComponent,
        PostListComponent,
        PostSaveComponent,
        ButtonComponent,
        GlyphComponent,
        ModalComponent,
        AlertComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [PostService, MessageService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
