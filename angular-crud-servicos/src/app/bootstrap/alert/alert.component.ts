import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MessageService} from "../../services/message.service";

declare let $;

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

    @Input()
    color = "success";

    @Input()
    close = false;

    @Input()
    timeout: null;

    @ViewChild('alertDiv')
    alertDiv: ElementRef;

    constructor(private messageService: MessageService) {
    }

    ngOnInit() {
        // Timeout to show message
        if (this.timeout) {
            setTimeout(() => {
                $(this.alertDiv.nativeElement).alert('close');
            }, this.timeout);
        }
    }

}
