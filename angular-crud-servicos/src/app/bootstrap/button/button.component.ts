import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() // allows data entry in the 'color' property. Ex: color="primary"
  color = 'danger';

  constructor() { }

  ngOnInit() {
  }

}
