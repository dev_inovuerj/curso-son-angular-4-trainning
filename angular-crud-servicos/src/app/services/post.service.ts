import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PostService {

    urlBase = 'https://jsonplaceholder.typicode.com/posts';

    constructor(private http: HttpClient) {
    }

    query(): Observable<Array<any>> {

        return this.http.get(this.urlBase);

        // .subscribe(data => this.posts = data);
    }

    save(data: any): Observable<any> {

        return !data.id
            ? this.http.post(this.urlBase, data)
            : this.http.put(`${this.urlBase}/${data.id}`, data);
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.urlBase}/${id}`);
    }

    destroy(id: number) { // status 204
        return this.http.delete(`${this.urlBase}/${id}`);
    }
}
