import {Component, OnInit} from '@angular/core';
import {PostService} from '../services/post.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from "../services/message.service";

@Component({
    selector: 'app-post-save',
    templateUrl: './post-save.component.html',
    styleUrls: ['./post-save.component.css']
})
export class PostSaveComponent implements OnInit {

    // fake instance Post Object
    post = {id: null, title: '', body: ''};
    saveModeTitle = 'Criar';

    constructor(private postService: PostService,
                private router: Router,
                private routeActivated: ActivatedRoute,
                private messageService: MessageService) {
    }

    ngOnInit() {

        // instead params (obsolete), use paramMap in the future
        this.routeActivated.params.subscribe(params => {

            if (params.hasOwnProperty('id')) {
                // call method load post
                this.postService.find(params.id).subscribe( post => {
                    this.saveModeTitle = 'Editar';
                    this.post = post;
                });
            }
        });

        // if(this.routeActivated.url)

    }

    save() {

        this.postService.save(this.post)
            .subscribe(data => {
                alert("Dados gravados com sucesso : \n\n" + JSON.stringify(data));
                this.messageService.body = "Data Saved Successfully";
                this.router.navigate(['/posts']);
            });
    }

}
