import { Injectable } from '@angular/core';
import { Task } from './task';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

const url_api = 'http://127.0.0.1:8000/tasks';

@Injectable()
export class TaskService {

  tasks:Array<Task> = [];	

  constructor(private http: Http) { }



  getTasks():Promise<Array<Task>>{
  	return this.http.get(url_api)
  		.toPromise()
  		.then( response=>response.json() );
  }

  createTask(task: Task){

  	return this.http.post(url_api,task)
  		.toPromise();

  }

}