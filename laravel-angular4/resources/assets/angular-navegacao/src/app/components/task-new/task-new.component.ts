import { Component, OnInit, Input } from '@angular/core';
import { Task } from '../../task';
import { TaskService } from '../../task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'task-new',
  templateUrl: './task-new.component.html',
  styleUrls: ['./task-new.component.css']
})
export class TaskNewComponent implements OnInit {

	@Input() tasks;

	task: Task = {
		name: "",
		value:0,
		date_launch:'2017-07-07'
	}

	add($event){

		$event.preventDefault();

		// Creating object copy(new Reference). 
		// New Object, same values
		// let taskCopy = Object.assign({},this.task);

		// this.tasks.push(taskCopy);
		this.taskService.createTask(this.task).then( ()=>{
			alert('Task Added!');
		} );

		this.routerService.navigate(['/tasks']);

	}



  constructor(
  	private taskService: TaskService,
  	private routerService: Router
  	) { 
  	// ref to tasks from TaskService
  	this.tasks = this.taskService.tasks;
  }

  ngOnInit() {

  }

}
