import { Component, OnInit, Input } from '@angular/core';

// Or Using Common Pipe Date in : https://angular.io/api/common/DatePipe
import { DatePipe } from '@angular/common';

import { Task } from '../../task';

import { TaskService } from '../../task.service';

// Decorator Turn on Component
@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

	@Input() tasks;

	constructor(private taskService: TaskService) { }

	ngOnInit() {
		
		this.taskService.getTasks().then( (tasks:Array<Task>)=>{
			this.tasks = tasks;
		});

	}

}
