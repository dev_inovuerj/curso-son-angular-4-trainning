<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', function () {
    return view('angular');
})->name('home');

/* Anything on matching app/? redirects to home */
Route::get('app/{any?}', function ($any='') {
    return redirect()->route('home');
});


// new route to Tasks
Route::resource('tasks','TasksController');