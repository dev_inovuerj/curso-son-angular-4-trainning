webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "h1{\n\t/* font-size: ; */\n\tcolor: orange;\n}\n\n.break-line {\n\tdisplay: table;\n\tborder: 1px solid grey;\n}\n\n.row {\n\tpadding-bottom: 15px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n\t<div class=\"row\">\n\n\t\t<div class=\"col-sm-12\">\n\t\n\t\t\t<h1>{{ title }}</h1>\n\n\t\t\t<!-- <task-new [tasks]=\"tasks\"></task-new> -->\n\t\t\t<!-- <task-list [tasks]=\"tasks\"></task-list> -->\n\n\t\t\t<button routerLink=\"/tasks\">Ver tarefas</button>\n\t\t\t<button routerLink=\"/task/new\">Adicionar Tarefa</button>\n\t\t\n\t\t</div>\n\n\t</div>\n\n\t<div class='row'>\n\t\t<div class=\"col-sm-12\">\n\t\t\t<router-outlet></router-outlet>\n\t\t</div>\n\t</div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// App Component Root

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Tarefas';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_task_list_task_list_component__ = __webpack_require__("../../../../../src/app/components/task-list/task-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_task_new_task_new_component__ = __webpack_require__("../../../../../src/app/components/task-new/task-new.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__filters_format_currency_pipe__ = __webpack_require__("../../../../../src/app/filters/format-currency.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__filters_format_date_pipe__ = __webpack_require__("../../../../../src/app/filters/format-date.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__directives_my_task_directive__ = __webpack_require__("../../../../../src/app/directives/my-task.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__directives_highlight_directive__ = __webpack_require__("../../../../../src/app/directives/highlight.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__task_service__ = __webpack_require__("../../../../../src/app/task.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// nG Core - @NgModule Decorator

// Forms - Added to apply binding, between property of forms and Components

// Components Imported



// Filters Imported


// Directives Imported


// Routes

// Services

var appRoutes = [
    {
        path: 'tasks', component: __WEBPACK_IMPORTED_MODULE_4__components_task_list_task_list_component__["a" /* TaskListComponent */]
    },
    {
        path: 'task/new', component: __WEBPACK_IMPORTED_MODULE_5__components_task_new_task_new_component__["a" /* TaskNewComponent */]
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_4__components_task_list_task_list_component__["a" /* TaskListComponent */],
                __WEBPACK_IMPORTED_MODULE_5__components_task_new_task_new_component__["a" /* TaskNewComponent */],
                __WEBPACK_IMPORTED_MODULE_6__filters_format_currency_pipe__["a" /* FormatCurrencyPipe */],
                __WEBPACK_IMPORTED_MODULE_7__filters_format_date_pipe__["a" /* FormatDatePipe */],
                __WEBPACK_IMPORTED_MODULE_8__directives_my_task_directive__["a" /* MyTaskDirective */],
                __WEBPACK_IMPORTED_MODULE_9__directives_highlight_directive__["a" /* HighlightDirective */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_router__["b" /* RouterModule */].forRoot(appRoutes)
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_11__task_service__["a" /* TaskService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/task-list/task-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "button{\n\tcolor: white;\n\tbackground: black;\n}\n\nul li {\n\tpadding: 5px;\n}\n\n.item-red {\n\tcolor: red;\n}\n\n.item-green {\n\tcolor: green;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/task-list/task-list.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Task List -->\n<ul>\n\n\n\n<li *ngFor=\"let item of tasks\" class=\"item-{{item.value<5 ? 'red' : 'green'}}\">\n\t\t{{item.name | uppercase}}\n\t\t{{item.name + '-' + item.value}}\n\t</li>\n\n\n</ul>"

/***/ }),

/***/ "../../../../../src/app/components/task-list/task-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__task_service__ = __webpack_require__("../../../../../src/app/task.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// Decorator Turn on Component
var TaskListComponent = (function () {
    // add($event){
    // 	$event.preventDefault();
    // 	// Creating object copy(new Reference). 
    // 	// New Object, same values
    // 	let taskCopy = Object.assign({},this.task);
    // 	this.tasks.push(taskCopy);
    // }
    function TaskListComponent(taskService) {
        this.taskService = taskService;
        this.task = { name: '', value: 0, date_launch: "2017-07-20" };
        this.today = Date.now();
    }
    TaskListComponent.prototype.ngOnInit = function () {
        console.log(this.tasks);
        /* Factoring Objects TaskService Manually without D.I (Bad Pattern) */
        this.tasks = this.taskService.tasks;
        // this.tasks.push({name:'Cozinhar', value:50, date_launch:'2017-07-07'})
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], TaskListComponent.prototype, "tasks", void 0);
    TaskListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'task-list',
            template: __webpack_require__("../../../../../src/app/components/task-list/task-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/task-list/task-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__task_service__["a" /* TaskService */]])
    ], TaskListComponent);
    return TaskListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/task-new/task-new.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "button{\n\tcolor: white;\n\tbackground: black;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/task-new/task-new.component.html":
/***/ (function(module, exports) {

module.exports = "<form (submit)=\"add($event);\">\n  \n\t<label for=\"name\">Name</label>\n\t<input id='name' type=\"text\" name=\"task[description]\" [(ngModel)]=\"task.name\">\n\n\t<br/>\n  \n\t<label for=\"value\">Value</label>\n\t<input id='value' type=\"text\" name=\"task[value]\" [(ngModel)]=\"task.value\">\n\n\t<br/>\n\t\n\t<label for=\"value\">Date</label>\n\t<input id='value' type=\"text\" name=\"task[date]\" [(ngModel)]=\"task.date_launch\">\n\n\t<br/>\n\n  \t<button type=\"submit\" (click)=\"add($event)\">Adicionar na lista</button>\n\n</form>"

/***/ }),

/***/ "../../../../../src/app/components/task-new/task-new.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskNewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__task_service__ = __webpack_require__("../../../../../src/app/task.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TaskNewComponent = (function () {
    function TaskNewComponent(taskService, routerService) {
        this.taskService = taskService;
        this.routerService = routerService;
        this.task = {
            name: "",
            value: 0,
            date_launch: '2017-07-07'
        };
        // ref to tasks from TaskService
        this.tasks = this.taskService.tasks;
    }
    TaskNewComponent.prototype.add = function ($event) {
        $event.preventDefault();
        // Creating object copy(new Reference). 
        // New Object, same values
        var taskCopy = Object.assign({}, this.task);
        this.tasks.push(taskCopy);
        this.routerService.navigate(['/tasks']);
    };
    TaskNewComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], TaskNewComponent.prototype, "tasks", void 0);
    TaskNewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'task-new',
            template: __webpack_require__("../../../../../src/app/components/task-new/task-new.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/task-new/task-new.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__task_service__["a" /* TaskService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], TaskNewComponent);
    return TaskNewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/directives/highlight.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HighlightDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
Symbols from Angular Core

Directive: provides the functionality of the @Directive decorator.
ElementRef: injects into the directive's constructor so the code can access the DOM element.

HostListener: Respond to user-initiated events. Allows you to listen for events in an element.
Input: allows data to flow from the binding expression into the directive.
*/

var HighlightDirective = (function () {
    /*Constructor*/
    function HighlightDirective(el) {
        this.el = el;
    }
    /*Events*/
    HighlightDirective.prototype.onMouseEnter = function () {
        // this.highlight(this.highlightColor);
        this.highlight(this.highlightColor || this.defaultColor || 'red');
    };
    HighlightDirective.prototype.onMouseLeave = function () {
        this.highlight(null);
    };
    /*Method Highlight*/
    HighlightDirective.prototype.highlight = function (color) {
        this.el.nativeElement.style.backgroundColor = color;
        this.el.nativeElement.className = 'break-line';
    };
    /*Hooks*/
    HighlightDirective.prototype.ngOnInit = function () {
        // from value of property color
        // this.highlight(this.highlightColor || 'red');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('myHighlight'),
        __metadata("design:type", String)
    ], HighlightDirective.prototype, "highlightColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HighlightDirective.prototype, "defaultColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('mouseenter'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], HighlightDirective.prototype, "onMouseEnter", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('mouseleave'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], HighlightDirective.prototype, "onMouseLeave", null);
    HighlightDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[myHighlight]' // Metadata 'Selector'. Customing a CSS selector for an attribute
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], HighlightDirective);
    return HighlightDirective;
}());



/***/ }),

/***/ "../../../../../src/app/directives/my-task.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyTaskDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__task__ = __webpack_require__("../../../../../src/app/task.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/* Hostlistener - bind Events between element DOM and Directives */


var MyTaskDirective = (function () {
    function MyTaskDirective(el) {
        this.el = el;
        this.el.nativeElement.innerHTML = 'Conteudo inserido';
    }
    Object.defineProperty(MyTaskDirective.prototype, "myTask", {
        /****************************************************************************/
        /*Watching values with Encapsules of Methods Magicals (getters And Setters) */
        /****************************************************************************/
        get: function () {
            return this._myTask;
        },
        set: function (value) {
            this._myTask = value;
            this.changeColorTask(); // value initialize in red color
        },
        enumerable: true,
        configurable: true
    });
    /* Bind Events*/
    MyTaskDirective.prototype.onClick = function () { alert(this.myTask); };
    /* Methods*/
    MyTaskDirective.prototype.changeColorTask = function () {
        this.el.nativeElement.style.color = this._myTask.value > 5 ? 'green' : 'red';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])() // pass value from template to directive
        ,
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__task__["a" /* Task */]),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__task__["a" /* Task */]])
    ], MyTaskDirective.prototype, "myTask", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], MyTaskDirective.prototype, "onClick", null);
    MyTaskDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[myTask]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], MyTaskDirective);
    return MyTaskDirective;
}());



/***/ }),

/***/ "../../../../../src/app/filters/format-currency.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatCurrencyPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Decorations and Transform Class

// Turn on a pipe |
var FormatCurrencyPipe = (function () {
    // Implements Interface PipeTransform to override 'transform' method
    function FormatCurrencyPipe() {
    }
    /*
        returns pattern currency BRL - Brazilian Real
     */
    FormatCurrencyPipe.prototype.transform = function (value, locale) {
        if (locale === void 0) { locale = 'pt-BR'; }
        var styleCurrency = {};
        styleCurrency['pt-BR'] = { style: 'currency', currency: 'BRL' };
        styleCurrency['en-US'] = { style: 'currency', currency: 'USD' };
        // ES 6 - Currency Formats (https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat)
        return new Intl.NumberFormat(locale, styleCurrency[locale]).format(value);
        // return null;
    };
    FormatCurrencyPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'formatCurrency' // name pipe. ex: <li {{item | formatCurrency}}>
        })
        // Implements Interface PipeTransform to override 'transform' method
    ], FormatCurrencyPipe);
    return FormatCurrencyPipe;
}());



/***/ }),

/***/ "../../../../../src/app/filters/format-date.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatDatePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FormatDatePipe = (function () {
    function FormatDatePipe() {
    }
    FormatDatePipe.prototype.transform = function (value, locale) {
        if (locale === void 0) { locale = "pt-BR"; }
        if (value.length < 10) {
            return value;
        }
        var dateArray = value.split('-');
        if (dateArray.length != 3) {
            return value;
        }
        var date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
        console.log(locale, date, dateArray);
        return Intl.DateTimeFormat(locale).format(date);
    };
    FormatDatePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'formatDate'
        })
    ], FormatDatePipe);
    return FormatDatePipe;
}());



/***/ }),

/***/ "../../../../../src/app/task.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TaskService = (function () {
    function TaskService() {
        this.tasks = [];
    }
    TaskService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "../../../../../src/app/task.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Task; });
var Task = (function () {
    function Task() {
    }
    return Task;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map